# Accepting Testing Exercise

## Prerequisites
- NodeJS
- Protractor

## Install Protractor
1. Run `npm install -g protractor`.
2. Update/install Selenium Server: `webdriver-manager update`

## Install CodeceptionJS
1. Run `npm install -g codeceptjs`.
2. You are ready to write your first acceptance test.

## Execute tests
1. Start Selenium Servier: `webdriver-manager start`
2. Start your local app: `gulp serve`
3. Verify your codecept.json is correctly configured. Your `helpers => Protractor => url` property must have the correct local URL and port.
4. Run the tests: `codeceptjs run --steps`

## Notes:
- AngularJS $timeout can cause problems with Protractor synchronization. Use $interval service for anything that polls continuously. https://github.com/angular/protractor/blob/master/docs/timeouts.md
